##### mono icons by Mono Company

Full ownership of Mono Icons goes to Mono Company.

- **Github:** [github.com/mono-company/mono-icons](https://github.com/mono-company/mono-icons)
- **Website:** [icons.mono.company](https://icons.mono.company) (`https` secure access expired as of Nov 2023)
- **Icons List:** [iconmagic.io/iconset/mi](https://iconmagic.io/iconset/mi)

**How to use:**

Include this after `<head>` in your site:
```html
<link href="https://monoicons.gitlab.io/i/icons.css" rel="stylesheet">
```

To place an icon, insert the following code where you want your icon to be:
```html
<i class="mono-icons" icon-name="shopping-cart"></i>
```

Replace `shopping-cart` (aka the content inside `icon-name=""`) with the name of the icon of your choosing.
